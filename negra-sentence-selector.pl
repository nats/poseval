#! /usr/bin/env perl
# Author: Arne Köhn <arne@arne-koehn.de>
# License: GPLv3 or later (see below)
#
# This script removes unneeded parts from a file that has negra format.
# output format: parseable by SVMTool (word tag\nword tag\n...)

package POSTaggerEvaluator::NegraProcess;
use strict;
use Getopt::Long;
use Pod::Usage;
use List::Util 'shuffle';

__PACKAGE__->main() unless caller;

sub main {
    my $infile;
    my $trainfilename;
    my $testfilename;
    my $goldfilename;
    my $startsentence;
    my $numsentences;
    my $pick_random;
    my $test_first;
    my $help;
    my $man;

    GetOptions('in=s'=> \$infile,
               'trainfile=s'=> \$trainfilename,
               'testfile=s' => \$testfilename,
               'goldfile=s' => \$goldfilename,
               'numtrain=i'=> \$numsentences,
	       'testfirst' => \$test_first,
               'random'=> \$pick_random,
               'help|?' => \$help,
               'man' => \$man
           );

    pod2usage(1) if $help;
    pod2usage(3) if $man;

    # this is useless atm. Could be used when the script schould write to
    # just one file.
    die "ERROR: testfile is useless if all sentences go to the trainfile!"
        if ( $testfilename && ! $numsentences);

    open(my $inputfile,  $infile) or die "couldn't open input file! Use --help for help\n";
    open(my $goldfile, '>', $goldfilename);
    open(my $testfile, '>', $testfilename);
    open(my $trainfile, '>', $trainfilename);

    __PACKAGE__->process($inputfile, $trainfile, $goldfile, $testfile,
                         $numsentences, $pick_random, 1, $test_first);

}

sub process {
    my $self = shift;
    my $inputfile = shift;
    my $trainfile = shift;
    my $goldfile = shift;
    my $testfile = shift;
    my $numsentences = shift;
    my $pick_random = shift;
    my $verbose = shift;
    my $test_first = shift;
    my $line = "1";
    # skip the header
    until ($line eq "#EOT SECEDGETAG\n") {
        die 'inputfile does not have a valid header!' if not $line = <$inputfile>;
    }

    # Read all sentences into an array
    my @sentences;
    $sentences[0] = '';
    my $n = 0;                  # the sentence number
    while ($line = <$inputfile>) {
        if ($line !~ /^[#*%].*/) {
#            $line =~ s/\t\$/\t/;
            $line = join(' ', (split(/\s+/, $line))[0,1])."\n";
            $sentences[$n] .= $line;
            if ($line =~ /\.$/) {
                $sentences[$n] .= "\n";
                $n++;
                $sentences[$n] = '';
            }
        }
    }

    if ($pick_random) {
        print STDERR "shuffling the sentences...\n" if $verbose;
        @sentences = shuffle(@sentences);
    }

    print STDERR "done reading $n sentences\n" if $verbose;

    print STDERR "Wrting $numsentences sentences to trainfile...\n" if $verbose;
    if ($test_first) {
	print $trainfile @sentences[$n-$numsentences+1..$n];
	print STDERR "Writing the goldfile...\n" if $verbose;
	print $goldfile @sentences[0..$n-$numsentences];
	print STDERR "Wrting the other sentences to testfile...\n" if $verbose;
	# delete the right answer - no cheating allowed!
	foreach my $f (@sentences[0..$n-$numsentences]) { $f =~ s/ .*//g }
	print $testfile @sentences[0..$n-$numsentences];
    }
    else {
	print $trainfile @sentences[0..$numsentences-1];
	print STDERR "Writing the goldfile...\n" if $verbose;
	print $goldfile @sentences[$numsentences..$n];
	print STDERR "Wrting the other sentences to testfile...\n" if $verbose;
	# delete the right answer - no cheating allowed!
	foreach my $f (@sentences[$numsentences..$n]) { $f =~ s/ .*//g }
	print $testfile @sentences[$numsentences..$n];
    }

}


__END__
=head1 negra-sentence-selector

This script gives you the ability to easily get sentences for test and
training out of a file that is in negra format.

=head1 SYNOPSIS

negra-sentence-selector.pl -in infile -trainfile trainfile -testfile testfile -goldfile goldfile  [options]

Options:

    -help:             brief help

    -man:              full help

    -numtrain [num]:   number of sentences for training. Rest is for testing

    -random:           choose sentences randomly

    -in [file]:        file to read the sentences from

    -trainfile [file]: This is where the training sentences go to

    -testfile [file]:  This is where the test sentences go to

    -goldfile [file]:  This is where the gold sentences go to

=head1 TRAIN-TEST-GOLD

The train file contains sentences with tags for training

The test file contains all other sentences without tags for testing

The gold file contains the same sentences as test, but with tags

=head1 AUTHOR

Arne Köhn, E<lt>arne@arne-koehn.eeE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 Arne Köhn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1
