#! /usr/bin/env python
# -*- coding:utf-8 -*-
# simple baseline tagger
# first argument: The lexicon (word SPACE tag\n ...)
# optional second argument: the default tag for unknown words
# Copyright (C) 2010 by Arne Köhn
# Feel free to do what you want to do with it.

import sys

lexfile = open(sys.argv[1])

fallbacktag = "NN"
if len(sys.argv)>2:
    fallbacktag = sys.argv[2]

lexicon = {}

for l in lexfile:
    word, tag = l.split()
    lexicon[word] = tag

while True:
    line = sys.stdin.readline()
    if line == "":
        exit(0) # eof
    word = line.strip()
    try:
        tag = lexicon[word]
    except KeyError:
        tag = fallbacktag
    if word <> "":
        print word+" "+tag
    else:
        print
