#! /usr/bin/env perl

# Copyright (C) 2010 by Arne Köhn

# Do what you want to to with this. If you need a "proper" license,
# have a look at LICENSE.

# assumes lines with tagged text.
# extracts lexicon of form:
# word t1 t2 t3 .. tn
# where t1 is most likely tag for word, t2 ... tn are other seen
# taggings in no particular order.

use strict;

my @temp;
my %seen;
my $key;
my $val;
my %bestval;
my %bestkey;
my %toprint;

while(<>) {
	if (! /^$/) {
	    @temp = split;
            ++$seen{$temp[0] . " " . $temp[1]};
	}
    }

while(($key,$val) = each %seen) {
    @temp = split(/\s+/,$key);
    if ($val > $bestval{$temp[0]}) {
	$bestkey{$temp[0]} = $temp[1];
	$bestval{$temp[0]} = $val;
    }
}

while (($key,$val) = each %bestkey) {
    print "$key $val\n"; }

