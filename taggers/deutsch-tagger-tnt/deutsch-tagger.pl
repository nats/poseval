#!/usr/bin/perl
# Copyright (C) 1997-2005 The CDG Team <cdg@nats.informatik.uni-hamburg.de>
#  
# This file is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read COPYING in the root of this distribution.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# cdg/utils/deutsch-tagger.pl
#
# Wrapper for a POS tagger of German. Different third-party
# taggers can be employed, and special heuristics are superimposed on
# their results to correct some common serious assignment errors.
use warnings;
use strict;

# decide which tagger to use
use Getopt::Std;
our($opt_m,$opt_n,$opt_t,$opt_b,$opt_q);
getopts 'bm:nt:q';
my $tagger = $opt_t ? $opt_t : 'tnt';
my $nocorrect = $opt_n ? 1 : 0;

# This script is general enough that it should be able to run without
# the full CDG suite.
our $use_db;
BEGIN {
#  use File::Basename;
#  unshift @INC, dirname(readlink($0) || $0);
#  if( do 'cdgsetup.pl' ) {
#    require CDG::German;
#    import CDG::German qw(all_cats);
#    $use_db = 1;
#  } else {
    $use_db = 0;
#  }
}

our $blocksize = 10000;

sub complain {
  my $m = shift;
  if ($m !~ /\n$/) {
    $m .= "\n";
  }
  print STDERR "WARNING: $m" unless $opt_q;
}

# Unambiguously decide for $pos
sub force {
  my($word,$pos) = @_;
  complain "`$$word{word}' is definitely $pos!\n";

  $$word{scores} = { $pos => 1};
  $$word{best} = $pos;
}

# raise score of @pos and lower previous best choice
sub prefer {
  my($word,@pos) = @_;
  complain "`$$word{word}' is probably @pos, not $$word{best}.\n";

  $$word{scores}{$$word{best}} /= 2;
  for my $p (@pos) {
    $$word{scores}{$p} = 1;
    $$word{best} = $p;
  }

}

# raise score of @pos to the of the best choice
sub alternate {
  my($word,@pos) = @_;
  complain "`$$word{word}' could also be @pos, not $$word{best}.\n";

   for my $p (@pos) {
     $$word{scores}{$p} = $$word{scores}{$$word{best}};
     $$word{best} = $p;
   }
}

# neutralize tagger scores for this word
sub equalize {
  my $word = shift;
  complain "`$$word{word}' is probably detected wrong.\n";
  for my $x (keys %{$$word{scores}}) {
    $$word{scores}{$x} = 1;
  }
  $$word{best} = '';

}

sub disprefer_finite_verb {
  my $x = shift;

  my $can_be_ADJA = 1;
  my $can_be_PP = 1;
  my $can_be_INF = 1;

  my $PP = 'VVPP';
  my $INF = 'VVINF';

  if($$x{best} eq 'VMFIN') {
    $PP = 'VMPP';
    $INF = 'VMINF';
  } elsif($$x{best} eq 'VAFIN') {
    $PP = 'VAPP';
    $INF = 'VAINF';
  }

  if($use_db) {
    my $value = all_cats(lc $$x{word});
    $can_be_ADJA = 0 unless $value =~ /ADJA/;
    $can_be_PP = 0 unless $value =~ /V.PP/;
    $can_be_INF = 0 unless $value =~ /V.INF/;
  } else {
    $can_be_ADJA = $$x{word} =~ /ten?$/;
    $can_be_PP = $$x{word} =~ /[nt]/;
    $can_be_INF = $$x{word} =~ /e[lr]?n$/;
  }

  if ($$x{best} =~ /V.FIN/ && $can_be_INF) {
    if($can_be_ADJA) {
      prefer $x, 'ADJA', $INF;
    } else {
      prefer $x, $INF;
    }
  } elsif ($can_be_PP) {
    prefer $x, $PP;
  } elsif ($can_be_ADJA) {
     prefer $x, 'ADJA';
   }
}

my %pronouns =
  (
   ich => '1sg',
   mir => '1sg',
   mich => '1sg',
   du => '2sg',
   dir => '2sg',
   dich => '2sg',
   er => '3sg',
   sie => '3sg',
   es => '3sg',
   wir => '1pl',
   uns => '1pl',
   ihr => '2pl',
   euch => '2pl'
  );
sub pernum {
  my $w = lc shift;
  if (exists $pronouns{$w}) {
    return $pronouns{$w};
  }
  return undef;
}


my %APZR;
for( qw (
       an:entlang am:entlang an:vorbei am:vorbei auf:herab auf:hin
       auf:zu aus:heraus aus:hinaus hinter:her hinter:hervor neben:her
       in:hinein um:herum um:willen unter:durch von:an von:aus von:her
       von:herab von:wegen vom:an vom:aus vom:her vom:herab vom:wegen
       vor:her zu:hin zur:hin �ber:hin �ber:hinaus �ber:hinweg
             �bers:hin �bers:hinaus �bers:hinweg ) ) {
  $APZR{$_}++;
}



# Use various patterns to remove obvious errors in the POS
# assignments
sub postprocess {
  my $x = shift;

  return unless @$x;

  my @words;
  my @phrases;

  # parse lines
  my $i = 0;
  for (@$x) {
    unless(s/^(\S+)\s+//) {
      push @words, { word => '' };
      next;
    }

    my $w = $1;
    my %s;
    my $record = 0;
    my $best = '';
    while (s/(\S+)\s+([0-9\.e+-]+)\s*//) {
      my ($pos,$x) = ($1,$2);
      $s{$pos} = $x;
      if ($x>$record) {
        $record = $x;
        $best = $pos;
      }
    }
    push @words, { nr => $i++, word => $w, scores => \%s, best => $best };
  }

  # cut into phrases
  my @p;
  my $j;
  for ($j = 0; $j <= $#words; $j++) {
    my $prev = $j ? $words[$j-1] : undef;
    my $this = $words[$j];
    my $next = $j < $#words ? $words[$j+1] : undef;

    # ignore quotation marks
    next if $$this{word} =~ /^[\'\"]$/;

    if ($$this{word} =~ /^([,.:;\-!?]|--+|\z)$/) {
      if (defined $$prev{best} && $$prev{best} eq 'ADJA' &&
          defined $$next{best} && $$next{best} eq 'ADJA') {
        push @p, $this;
      } else {
        push @phrases, [@p] if @p;
        @p=();
      }
    } else {
      push @p, $this;
    }
  }
  push @phrases, [@p] if(@p);

  # Apply error patterns
  unless($nocorrect) {

    # Words composed solely of punctuation characters are punctuation.
    # No one ought to be allowed to call their company that...
    for my $w (@words) {
      if ($$w{word} =~ /^[\[\]]$/ && $$w{best} ne '$(') {
        force $w, '$(';
      } elsif ($$w{word} =~ /^[.!?]+$/ && $$w{best} ne '$.') {
        force $w, '$.';
      } elsif ($$w{word} =~ /^[a-z]\)$/ && $$w{best} ne 'XY') {
        # Do not assume that paragraph marks such as `a)' are proper names.
        force $w, 'XY';
      }
    }

    # Roman ordinal numerals are ADJA.
    for my $w (@words) {
      if ($$w{word} =~ /^[XMCLDVI]{2,}\.$/i && $$w{best} ne 'ADJA') {
        force $w, 'ADJA';
      }
    }

    # Compound numbers, such as III-167, are CARD.
    for my $w (@words) {
      if ($$w{word} =~ /^[XMCLDVI]+-[0-9]+$/ && $$w{best} ne 'CARD') {
        force $w, 'CARD';
      }
    }

    # Words that we know to be a particular class are that class.
    #
    # This is much more complicated than it sounds. CDG will ignore
    # tagger assignments that its lexicon does not cover, so if the
    # tagger does not know the word `gestylt'/VVPP and assigns VVFIN to
    # it, but the lexicon knows it is VVPP, cdg will complain and ignore
    # the impossible prediction.
    #
    # But realistic grammars have to deal with unknown words; and so it
    # is likely that `gestylt'/FM *is* covered by the lexicon after all,
    # through the use of a template. This is a double-edged sword. For
    # instance, pretty much any word might be `FM', even if we know it
    # only as, say, an ART. On the other hand, particular forms that
    # *are* known radically decrease the probability that there is an
    # unknown word of a different class with the same reading. An
    # example: stock TnT does not know the word `abgek�rzt' and assumes
    # it is VVFIN - but it is unambiguously VVPP, and we can be pretty
    # sure that it is *not* a form of a verb `abgek�rzen' that we happen
    # to have overlooked. It is such errors that we try to correct with
    # this rule.
    if ($use_db) {
      for my $w (@words) {
        next unless $$w{word};

        my $value = all_cats($$w{word}) || '';
        my $altvalue = '';
        if (0 == $$w{nr}) {
          my $lowword = lc $$w{word};
          $altvalue = all_cats($lowword) || '';
        }
        # TnT sometimes judges words as KOUS that are provably
        # something different.
        if ($$w{best} =~ /^KO/ &&
            $value !~ /KO/ &&
            $altvalue !~ /KO/) {
          complain "`$$w{word}' can't possibly be $$w{best}!\n";
          equalize $w;
        }

        # TnT calls some words such as `mancher' PIDAT,
        # when actually they can only be PIAT.
        elsif($$w{best} eq 'PIDAT' &&
              $value !~ /PIDAT/ &&
              $value =~ /PIAT/) {
          prefer $w, 'PIAT';
        }

        # It is *very* unlikely that a form that we know as a verb
        # form is actually an unknown word.
        elsif ($value =~ /V[AMV]|APPR|ADV|PWAV/ &&
               $value !~ /$$w{best}/) {
          complain "Hey, I know word `$$w{word}!'";
          $value =~ s|/.*||g;
          $value =~ s|[\(\)]||g;
          prefer $w, split(/\|/,$value);
        }

        # If we know a word as FM (which applies only to very few
        # items), give it a chance to actually be FM. TnT practically
        # never predicts FM correctly.
        elsif ($value =~ /FM/ &&
               $$w{word} =~ /^[A-Z]/ &&
               $$w{best} ne 'FM') {
          alternate $w, 'FM';
        }
      }
    }

    # Maybe undo spurious initial upcase
    if ($use_db) {

      # Find first proper word
      my $i = 0;
      while($i < $#words &&
            $words[$i]{word} =~ /^[\"\'(\[\/]/) {
        $i++;
      }
      if($words[$i]{word} =~ /^[A-Z���]/ &&
         $words[$i]{best} =~ /^(NN|NE|FM)$/) {
        my $low = lc $words[$i]{word};
        $low =~ tr/���/���/;
        my $v = all_cats($low);
        if ($v) {
          complain "Hey, $low is a known word: $v!";
          $v =~ s|/.*||g;
          $v =~ s|[\(\)]||g;
          for (split(/\|/, $v)) {
            s!/.+!!;
            alternate $words[$i], $_;
          }
        }
      }
    }

    # InternalUpperCase is the plague that marketing copywriters have
    # foisted on us. But at least it is a surefire indicator of the word
    # class NE.
    for my $w (@words) {
      if ($$w{word} =~ /[a-z][A-Z][^-\/]*$/ &&
          $$w{word} !~ /^(.*In(nen)|GmbH)?$/ &&
          $$w{best} ne 'NE') {
        force $w, 'NE';
      }
    }

    # `noch' is ambiguous between KON and ADV, but it is certainly KON
    # when `weder' precedes, no matter how far back.
    my $weder = 0;
    for my $w (@words) {
      if ($$w{word} =~ /^weder$/i) {
        $weder = 1;
      }
      if ($weder &&
          $$w{word} =~ /^noch$/i) {
        $weder = 0;
        if ($$w{best} ne 'KON') {
          complain "Conjunction `weder' followed by `noch'/ADV\n";
          prefer $w, 'KON';
        }
      }
    }

    # Imperatives do not occur in direct questions.
    my $question = 0;
    for my $w (reverse @words) {
      if ($$w{word} eq '?') {
        $question = 1;
      }
      if ($$w{word} =~ /^[!\.:;]$/) {
        $question = 0;
      }
      if ($question && $$w{best} =~ /IMP/) {
        complain "Imperative `$$w{best}' followed by question mark!\n";
        (my $fin = $$w{best}) =~ s/IMP/FIN/;
        prefer $w, $fin;
      }
    }

    for my $p (@phrases) {

      my @w = @$p;

      # `nach wie vor' is consistently mis-tagged
      for (2..$#w) {
        if ($w[$_-2]{word} =~ /^nach$/i &&
            $w[$_-1]{word} eq 'wie' &&
            $w[$_]{word} eq 'vor') {
          force $w[$_-2], 'ADV';
          force $w[$_-1], 'KOKOM';
          force $w[$_], 'ADV';
        }
      }

      # Words like `mich' are ambiguous between a reflexive and a
      # non-reflexive reading. There is no syntactic way of distinguishing
      # them; but if the corresponding personal pronoun is in the
      # vicinity, the reflexive form is much more likely.
      for (0..$#w) {
        if ($w[$_]{word} =~ /^(mich|mir|dich|dir|uns|euch)$/i) {

          # after imperatives, `dich'/`dir' are invariably right.
          if ($_ > 0 &&
              $w[$_]{word} =~ /^[dD]/ &&
              $w[$_-1]{best} =~ /IMP/) {
            if ($w[$_]{best} eq 'PPER') {
              complain "Personal `$w[$_]{word}' follows imperative!";
              prefer $w[$_], 'PRF';
            }
            next;
          }

          # find previous personal pronoun
          my $this = pernum($w[$_]{word});
          my $i = $_-1;
          while ($i >= 0) {
            my $that = pernum($w[$i]{word});
            if ($that && $that ne $this &&
                $w[$_]{best} eq 'PRF') {
              complain "Reflexive `$w[$_]{word}' near `$w[$i]{word}!'";
              prefer $w[$_], 'PPER';
              last;
            }
            if ($that && $that eq $this &&
                $w[$_]{best} eq 'PPER') {
              complain "Personal `$w[$_]{word}' near `$w[$i]{word}!'";
              prefer $w[$_], 'PRF';
              last;
            }
            $i--;
          }
        }
      }

      # Imperatives do not follow most other words.
      for (1..$#w) {
        if ($w[$_]{best} =~ /IMP/ &&
            $w[$_-1]{best} !~ /^(KON|ITJ|PTKANT|ADV|\$\()$/) {
          complain "Imperative following `$w[$_-1]{word}'!";
          (my $fin = $w[$_]{best}) =~ s/IMP/FIN/;
          prefer $w[$_], $fin;
        }
      }

      # PTKZU can only precede infinitives, active verbal adjectives, or
      # punctuation.
      for (0..$#w-1) {
        if ($w[$_]{best} eq 'PTKZU' &&
            $w[$_+1]{best} !~ /INF|\$|ADJA/i) {
          complain "Particle `zu' not followed by infinitive!";
          prefer $w[$_], 'APPR';
        }
      }

      # TnT is smart enough to know that PTKZU ADJA is a
      # feasible pattern, but it cannot distinguish between the
      # plausible `zu/PTKZU versteuernde/ADJA' and the impossible
      # `zu/PTKZU st�rkere/ADJA'. Forbid that latter option.
      for (0..$#w-1) {
        if ($w[$_]{word} =~ /^zu$/i &&
            $w[$_+1]{best} eq 'ADJA' &&
            $w[$_+1]{word} !~ /nde/) {
          complain "PTKZU followed by non-participial adjective!";
          prefer $w[$_], 'APPR', 'PTKA';
        }
      }

      # `zu' in front of an active participle is almost certainly
      # PTKZU, not PTKA: "der zu zahlende Betrag"
      for (0..$#w-1) {
        if ($w[$_]{word} =~ /^zu$/i &&
            $w[$_]{best} ne 'PTKZU' &&
            $w[$_+1]{best} =~ /ADJA/ &&
            $w[$_+1]{word} =~ /e[rl]?nde[nmsr]?$/) {
          complain "Adjectival `zu' in front of active participle!";
          prefer $w[$_], 'PTKZU';
        }
      }

      # `am' in front of `-sten' is almost certainly PTKA.
      for (0..$#w-1) {
        if ($w[$_]{word} =~ /^am$/i &&
            $w[$_]{best} ne 'PTKA' &&
            $w[$_+1]{word} =~ /[�s]ten$/) {
          complain "Preposition `am' in front of superlative!";
          prefer $w[$_], 'PTKA';
          prefer $w[$_+1], 'ADJD';
        }
      }

      # `wie' within a clause is probably KOKOM, not PWAV.
      for (1..$#w-1) {
        if ($w[$_]{word} =~ /^wie$/i &&
            $w[$_]{best} ne 'KOKOM' &&
            $w[$_+1]{best} !~ /ADJ|ADV|^VV/ &&
            $w[$_-1]{best} !~ /KON|\$\(/) {
          complain "wie/PWAV within a clause!";
          prefer $w[$_], 'KOKOM';
        }
      }

      # Some verb forms look like adjectives. But if they are introduced
      # by a subordinating conjunction, they are almost certainly not
      # adjectives.
      if ($w[0]{best} eq 'KOUS' &&
          $w[-1]{best} eq 'ADJD' &&
          exists $w[-1]{scores}{VVFIN}) {
        complain "Subclause with `$w[0]{word}' ends with deverbal adjective!\n";
        prefer $w[-1], 'VVFIN';
      }

      # `bis', `w�hrend' etc are ambiguous between KOUS and APPR.
      # If an infinitive ends the subclause, APPR is more likely;
      # if a finite verb ends it, KOUS is more likely.
      if ($w[0]{word} =~ /^(bis|seit|statt|w�hrend|so(weit|lange|viel|wenig))$/i) {
        if ($w[0]{best} eq 'KOUS' &&
            $w[-1]{best} =~ /INF/) {
          complain "Subclause with `$w[0]{word}' ends with infinitive!\n";
          prefer $w[0], 'APPR';
        } elsif ($w[0]{best} =~ /APPR|ADV/ &&
                 $w[-1]{best} =~ /FIN/) {
          complain "PP with `$w[0]{word}' ends with finite verb!\n";
          prefer $w[0], 'KOUS';
        }
      }

      # `anstatt' etc. are ambiguous between KOUI and APPR. But KOUI is
      # unlikely if there is no `zu' to be found.
      if ($w[0]{word} =~ /^(um|ohne|statt|anstatt|ausser|au�er)$/i) {
        if ($w[0]{best} eq 'KOUI' && $w[1]{word} !~ /^da(�|ss)$/) {
          my $zu = undef;
          for (0..$#w) {
            if ($w[$_]{word} =~ /zu/) {
              $zu = $w[$_];
              last;
            }
          }
          if (!defined $zu) {
            complain "`$w[0]{word}' has no `zu'!\n";
            prefer $w[0], 'APPR';
          }
        }

        # Contrariwise, if there *is* an IZU following, KOUI is much more likely.
        elsif ($w[0]{best} eq 'APPR') {
          for (0..$#w) {
            if ($w[$_]{best} =~ /PTKZU|IZU/) {
              complain "`$w[0]{word}' followed by `zu'!\n";
              prefer $w[0], 'KOUI';
              last;
            }
          }
        }
      }

      # `ihr' is unique in being PPER/PPOSAT ambiguous, and TnT
      # says PPOSAT far too often, even at the end of clauses.
      for (0..$#w) {
        if($w[$_]{word} eq 'ihr' &&
           $w[$_]{best} eq 'PPOSAT') {
          my $kernel;
          my $wrong_ADJA;
          for ($_..$#w) {
            if ($w[$_]{best} eq 'ADJA' && $w[$_]{word} !~ /e[sr]$/) {
              $wrong_ADJA = $w[$_];
              last;
            }
            if ($w[$_]{best} =~ /NN|NE|FM|TRUNC|PIS/) {
              $kernel = $w[$_];
              last;
            }
          }
          if(!defined $kernel || defined $wrong_ADJA) {
            complain "PPOSAT without kernel!\n";
            prefer $w[$_], 'PPER';
          }
        }
      }

      # `mit', `ab' and others are ambiguous as APPR/PTKVZ. The category
      # PTKVZ is particularly hard to get right. But if a verb follows,
      # it is almost certainly not a preposition.
      for (0..$#w-1) {
        if ($w[$_]{best} =~ /^APPR/ &&
            $w[$_+1]{best} =~ /^V/) {
          complain "Suspicious preposition followed by verb!";
          equalize $w[$_];
        }
      }

      # All PTKVZ look like APPR; but if no possible PP kernel follows,
      # APPR is more likely.
      for (0..$#w) {
        if ($w[$_]{best} eq 'APPR') {
          my $kernel = undef;
          my $vvfin = undef;
          for my $i (0..$_-1) {
            if ($w[$i]{best} =~ /FIN/) {
              $vvfin = $w[$i];
              last;
            }
          }
          for $i($_..$#w) {
            if ($w[$i]{best} =~ /^(XY|NN|NE|FM|PIS|ADJ[AD]|PPER|PRF|ADV|CARD|TRUNC|VVPP|PROAV)$/) {
              $kernel = $w[$i];
              last;
            }
          }
          if (defined $vvfin && !defined $kernel) {
            complain "Preposition `$w[$_]{word}' without kernel!\n";
            prefer $w[$_], 'PTKVZ';
          }
        }
      }

      # initial `doch', `denn', `aber' are virtually always KON, not ADV.
      if ($w[0]{word} =~ /^(denn|aber|doch|jedoch)$/i &&
          $w[0]{best} eq 'ADV') {
        complain "Adverb `$w[0]{word}' starts phrase\n";
        prefer $w[0], 'KON';
      }

      # The sequence `�ber'+CARD is always tagged as APPR, but it is
      # much more often an ADV. The same goes for some other
      # prepositions/adverbs.
      for (0..$#w-1) {
        if ($w[$_]{word} =~ /^(unter|[��]ber|zwischen)$/i &&
            $w[$_+1]{best} =~ /CARD/) {
          complain "Suspicious preposition followed by number!";
          alternate $w[$_], 'ADV';
        }
      }

      # Successive articles are nearly always wrong.
    WORD:
      for (0..$#w-2) {
        my($l,$r) = ($w[$_], $w[$_+1]);
        if ($$l{best} eq 'ART' &&
            $$r{best} eq 'ART') {

          # There are really only two cases where this is legit:
          #
          # 1. `der ein oder andere'.
          #
          if ($$l{word} =~ /^der$/i &&
              $$r{word} =~ /^ein$/i &&
              $w[$_+2]{word} =~ /^oder$/i) {
            next WORD;
          }

          # 2. Truly nested NPs. These are always formed with an adjective
          # that licences the inner NP:
          #
          #  `die den Kunstwerken entsprechende Analyse ist...'
          #
          my $ADJA = 0;
        SEARCH:
          for my $x ($_+1..$#w) {
            if ($w[$x]{best} =~ /^N[NE]$/) {
              for my $y ($x+1..$#w) {
                if ($w[$y]{best} =~ /FIN$/) {
                  last SEARCH;
                }
                if ($w[$y]{best} eq 'ADJA') {
                  next WORD;
                }
              }
            }
          }

          # Everything else is almost certainly a classification error.
          complain 'Duplicate articles!';
          if ($$l{word} =~ /^d/i && $$r{word} =~ /^eine[nm]?$/i) {
            prefer $r, 'ADJA';
          } elsif ($$l{word} =~ /^d/i) {
            prefer $l, 'PDS';
          } else {
            prefer $l, 'PIS';
          }
        }
      }

      # The sequence P..AT ART is possible, but only for some
      # restricted constructions.
      for (0..$#w-1) {
        my($l,$r) = ($w[$_], $w[$_+1]);
        if ($$l{best} =~ /^P.+AT$/ &&
            $$r{best} =~ /^(ART|P.+AT|V|PTKZU)/) {

          # `manch/PIDAT ein/ART Unternehmen'
          next if $$l{word} =~ /^(manch|solch|welch)$/i && $$r{word} =~ /^ein/;

          # `die/ART beiden/PIDAT Schwestern'
          next if $$r{word} =~ /^(all|beid)/;

          # `dessen/PRELAT eines/ART Ende'
          next if $$l{best} eq 'PRELAT' && $$r{word} =~ /^e/;

          # `diese/PDAT eine/ART Szene'
          next if $$l{best} eq 'PDAT' && $$r{word} =~ /^e/;

          # `weil wir alle/PIDAT die/ART Verfassung achten'
          next if $$l{word} eq 'alle' && $w[$_-1]{best} eq 'PPER';

          # `mit seinen/PPOSAT die/ART Blechinstrumente/NN anfeuernden/NN F�usten'
          my($NN,$participle);
          for my $j ($_+2..$#w) {
            if ($NN &&
                $w[$j]{best} eq 'ADJA' &&
                $w[$j]{word} =~ /nde[nrms]?$/) {
              $participle = $w[$j];
              last;
            }
            if ($w[$j]{best} =~ /^[NP]/) {
              $NN = $w[$j];
            }
          }
          next if $participle;

          complain "Attributive pronoun precedes non-NP material!";
          if ($$l{word} =~ /^ihr$/i) {
            prefer $l, 'PPER'
          } elsif ($$l{word} =~ /^(dies|jen|dess)/i) {
            prefer $l, 'PDS'
          } elsif ($$l{word} =~ /^wenig/i) {
            prefer $l, 'PIS'
          } elsif ($$l{word} =~ /^w/i) {
            prefer $l, 'PWS'
          } else {
            prefer $l, 'PIS'
          }
        }
      }

      # The sequence ART V.FIN is certainly wrong. Either the article is
      # a substituting pronoun, or the finite verb is something else.
      for (0..$#w-1) {
        if ($w[$_]{best} eq 'ART' &&
            $w[$_+1]{best} =~ /FIN/) {
          complain "Article precedes finite verb\n";

          if ($w[$_]{word} =~ /^e/) {
            alternate $w[$_], 'PIS';
          }

          if ($w[$_+1]{word} =~ /^(.+-)?[A-Z]/) {
            alternate $w[$_+1], 'NN';
          } elsif ($w[$_+1]{word} =~ /en?$/) {
            alternate $w[$_+1], 'ADJA';
          } elsif ($w[$_+1]{word} =~ /[nt]$/) {
            (my $p = $w[$_+1]{best}) =~ s/FIN/PP/;
            alternate $w[$_+1], $p;
          }
        }
      }

      # Now for the confusion between articles and relative pronouns. If
      # an article opens and an infinite verb and a finite verb end a
      # phrase, then the article is usually a relative pronoun really.
      if ($w[0]{nr} != 0 &&
          $w[0]{best} eq 'ART' &&
          $w[0]{word} =~ /^(der|die|das|den|dem)$/ &&
          $#w > 0 &&
          $w[-2]{best} =~ /^V/ &&
          $w[-1]{best} =~ /FIN/) {
        complain "Article starts subclause\n";
        prefer $w[0], 'PRELS';
      }

      # A relative pronoun can also be second in a clause if a
      # preposition precedes.
      if ($w[0]{nr} != 0 &&
          $#w > 0 &&
          $w[0]{best} eq 'APPR' &&
          $w[1]{word} =~ /^(der|die|das|den|dem)$/ &&
          $w[1]{best} eq 'ART' &&
          $w[-1]{best} =~ /(FIN|INF)$/) {

        # search intervening PTKZU or FIN
        my $saw_fin = 0;
        for(1..$#w-1) {
          if($w[$_]{best} =~ /PTKZU|FIN$/) {
            $saw_fin = 1;
            last;
          }
        }
        if(!$saw_fin) {
          complain "Suspicious APPR+ART+...+FIN!\n";
          prefer $w[1], 'PRELS';
        }
      }

      # An article that starts a phrase that ends with a finite verb
      # is also often really a PRELS, but obviously not if it is the
      # first word in the sentence.
      if ($w[0]{nr} > 0 &&
          $w[0]{best} eq 'ART' &&
          $w[0]{word} =~ /^(der|die|das|den|dem)$/ &&
          $#w > 0 &&
          $w[-1]{best} =~ /FIN/) {

        # Don't do this if there is another finite verb within the phrase.
        my $saw_verb = 0;
        for(1..$#w-1) {
          if($w[$_]{best} =~ /FIN|PTKZU/) {
            $saw_verb = 1;
            last;
          }
        }
        if(!$saw_verb) {
          complain "Article starts subclause\n";
          alternate $w[0], 'PRELS';
        }
      }

      # Articles rarely end phrases.
      if ($w[-1]{best} eq 'ART') {
        complain "Article ends subclause!\n";
        if ( $w[-1]{word} =~ /^(der|die|das|den|dem)$/) {
          prefer $w[-1], 'PDS';
        } else {
          prefer $w[-1], 'PIS';
        }
      }

      # Another sign of trouble is an infinitive that occurs on its own,
      # with no other verb near. This is almost certainly a finite form instead.
      my $inf = '';
      my $next = '';
      my $verbs = 0;
      for (@w) {
        $verbs++ if $$_{best} =~ /^V|PTKZU/;
        $inf = $_ if $$_{best} =~ /INF/;
      }
      if ($inf &&
          1 == $verbs &&
          $$inf{nr} < $w[-1]{nr} &&
          $words[$$inf{nr}+1]{best} ne 'KOKOM' ) {
        complain "Lonely infinitive `$$inf{word}'\n";

        (my $fin = $$inf{best}) =~ s/INF/FIN/;
        prefer $inf, $fin;
      }

      # Much the same goes for participles.
      my $pp = '';
      $verbs = 0;
      for (@w) {
        $verbs++ if $$_{best} =~ /^V/;
        $pp = $_ if $$_{best} =~ /^V.PP$/;
      }
      if ($pp &&
          1 == $verbs &&
          $$pp{nr} < $w[-1]{nr}) {
        complain "Lonely participle `$$pp{word}'\n";

        (my $fin = $$pp{best}) =~ s/PP/FIN/;
        prefer $pp, $fin;
      }

      # Another characteristic error occurs when the finite form
      # signalled by an initial subordinating conjunction is misdetected
      # as an infinitive or participle.
      if ($w[0]{best} =~ /PREL|KOUS|PWAV/ ||
          defined $w[1] && $w[1]{best} =~ /PREL|KOUS/) {

        # There are, of course, true WH questions, in which
        # PWAV starts and a participle ends the phrase:
        #
        # `Warum hast du das nicht gleich gesagt?'
        #
        # These are characterized by a finite verb in the second position.
        if (!(defined $w[1] && $w[1]{best} =~ /FIN/)) {

          if ($w[-1]{best} =~ /INF$/ && $w[-2]{word} ne 'zu') {
            (my $fin = $w[-1]{best}) =~ s/INF/FIN/;
            if(!($use_db && all_cats($w[-1]{word}) !~ /$fin/)) {
              complain "Infinitive `$w[-1]{word}' ends subclause\n";
              prefer $w[-1], $fin;
            }
          } elsif ($w[-1]{best} =~ /PP$/  
              # && $w[-1]{word} !~ /^ge/   # This condition can be omitted because
                                           # a lookup is done whether the verb
                                           # starting with "ge" can be finite
                                        ) {
            (my $fin = $w[-1]{best}) =~ s/PP/FIN/;
            if(!($use_db && all_cats($w[-1]{word}) !~ /$fin/)) {
              complain "Participle `$w[-1]{word}' ends subclause\n";
              prefer $w[-1], $fin;
            }
          }
        }
      }

      # Conversely, modal verbs signal inifinitives.
      for (0..$#w) {

        # If there is a modal verb...
        if ($w[$_]{best} eq 'VMFIN') {

          # followed by a participle that could be an infinitive...
          for my $i($_+1 .. $#w) {
#            if($w[$i]{best} =~ /^(VVPP|ADJA|VVFIN)$/ &
#               $use_db &
#               all_cats($w[$i]{word}) =~ /VVINF/) {
#
#              # ...then the participle really is an infinitive,
#              # unless another auxiliary verb follows
#              my $all = all_cats($w[$i+1]);
#              if($i < $#w &&
#                 all_cats($w[$i+1]{word}) =~ /^VA/) {
#                next;
#              }
#              complain "Modal `$w[$_]{word}' followed by participle\n";
#              prefer $w[$i], 'VVINF';
#              last;
#            }
          }
        }
      }

      # A sure sign of trouble is two finite verbs not separated by any
      # phrase boundary or conjunction. This is almost certainly wrong,
      # but hard to correct because there are three different common
      # reasons:
      #
      # - passive verbal adjectives: `_verkaufte_ Ger�te'
      # - infinitives: `hat viele Abonnements verkaufen _k�nnen_'
      # - participles with unstressed prefixes: `hat den Pr�sidenten beraten'
      my $left = '';
      my $right = '';
      my @kon = ();
      for (@w) {

        # intervening KON can explain why there are two finite verbs
        if ($$_{best} =~ /KOUS|KON|\$\(/) {
          $kon[$$_{nr}] = 1;
        }

        if ($$_{best} =~ /FIN/) {

          # second finite verb detected
          if ($left) {
            $right = $_;

            # If there was a KON between the two verbs, it is probable
            # that it joins the two verbs IF it is adjacent to either of
            # them, but not otherwise.
            if ($kon[$$left{nr}+1] || $kon[$$right{nr}-1]) {
              next;
            }

            complain "Two finite words: `$$left{word}' and `$$right{word}'\n";

            # if one of the two is an auxiliary verb, try to change the
            # other tag.
            if ($$left{best} =~ /^V[MA]/) {
              disprefer_finite_verb $right;
            } elsif ($$right{best} =~ /^V[MA]/) {
              disprefer_finite_verb $left;
            }

            # if one of the two verbs can only be a finite verb,
            # then force the other to be non-finite.
            elsif($use_db && all_cats($$left{word}) =~ /^V.FIN$/) {
              disprefer_finite_verb $right;
            }
            elsif($use_db && all_cats($$right{word}) =~ /^V.FIN$/) {
              disprefer_finite_verb $left;
            }

            # otherwise just assume that the right verb is in fact not finite
            else {
              disprefer_finite_verb $right;
            }
          }

          # first finite verb detected
          else {
            $left = $_;
          }
        }
      }

      # A relative pronoun is always followed by a clause-final finite verb.
      if($w[0]{best} eq 'PRELS' &&
        $w[0]{word} =~ /^d/) {

        # find clause-final verb
        my $found = 0;
        for my $i ($w[0]{nr}+1..$#words-1) {
          if($words[$i]{best} =~ /FIN$/ &&
             ($i == $#words-1 ||
              $words[$i+1]{best} =~ /\$|KON/)) {
            $found = 1;
            last;
          }
        }
        if(!$found) {
          complain "PRELS without finite verb\n";
          prefer $w[0], 'ART';
        }
      }

      # `als' in front of a finite Verb is certainly KOKOM, not KOUS.
      for (0..$#w-1) {
        if ($w[$_]{word} =~ /^als$/i &&
            $w[$_]{best} ne 'KOKOM' &&
            $w[$_+1]{best} =~ /FIN/) {
          complain "KOUS followed by finite verb!";
          prefer $w[$_], 'KOKOM';
        }
      }

      # Anyway, `als' is never APPR; it is either KOKOM or KOUS.
      for (0..$#w-1) {
        if ($w[$_]{word} =~ /^als$/i &&
            $w[$_]{scores}{APPR} > 0) {
          complain "`als' tagged as APPR!";

          $w[$_]{scores}{KOKOM} = $w[$_]{scores}{APPR};
          $w[$_]{scores}{APPR} = 0;

          # non-initial `als' is mostly KOKOM
          if ($_ > 0) {
            force $w[$_], 'KOKOM';
          }

          # otherwise, `als' that ends with a finite verb is KOUS
          elsif ($w[-1]{best} =~ /FIN/) {
            prefer $w[$_], 'KOUS';
          }

          # otherwise, assume KOKOM
          else {
            prefer $w[$_], 'KOKOM';
          }
        }
      }

      # `etwas' and some related words are ambiguous between ADV and
      # PIS. In front of other adverbials they are more often PIS.
      for (0..$#w) {
        if ($w[$_]{word} =~ /^(weniger|etwas)$/i
            && $w[$_]{best} eq 'ADV'
            && !($_ > 0 && $w[$_-1]{word} eq 'nicht')
            && ($_ == $#w || $w[$_+1]{best} =~ /APPR|VVPP|PROAV/)) {
          complain "Suspicious ADV before adverbial!";
          prefer $w[$_], 'PIS';
        }
      }

      # Particles which could be circumpositions are probably
      # circumpositions if a fitting preposition precedes.
      my $PTKVZ = '';
      for (reverse @w) {
        $PTKVZ = $_ if $$_{best} eq 'PTKVZ';
        $PTKVZ = '' if $$_{best} =~ /FIN/;
        if ($PTKVZ &&
            $$_{best} =~ /^APPR/ &&
            exists($APZR{"$$_{word}:$$PTKVZ{word}"})) {
          complain "`$$_{word}' ... `$$PTKVZ{word}' probably belongs together.";
          alternate $PTKVZ, 'APZR';
        }
      }

      # PRELAT occurs only after a comma. Things get more complicated,
      # though, because both prepositions and conjunctions can intervene.
      for (1..$#w) {
        if ($w[$_]{word} =~ /^de(r|ss)en$/ &&
            $w[$_]{best} eq 'PRELAT') {
          my $prev = $w[$_-1];
          my $verb = undef;
          if ($_ > 1) {
            $verb = $w[$_-2];
          }
          if ($$prev{best} eq 'APPR' && $_ > 1) {
            $prev = $w[$_-2];
            if ($_ > 2) {
              $verb = $w[$_-3];
            }
          }
          unless (($$prev{word} =~ /^(und|oder)$/ &&
                   defined $verb && $$verb{best} =~ /FIN/)
                  ||
                  $$prev{best} eq 'APPR' && $_ == 1) {
            complain "$w[$_]{word} without comma!\n";
            prefer $w[$_], 'PDAT';
          }
        }
      }

      # TnT has great trouble with `derjenige' etc.; it always assumes
      # that it is unambiguously PDS. Assume it can be PDAT in front of
      # nominal material.
      for (0..$#w -1) {
        if ($w[$_]{word} =~ /^(der|die|das|des|dem|den|wie)(viel|jenig|selb)e[srnm]?$/i &&
            $w[$_]{best} =~ /^P[DW]S$/ &&
            $w[$_+1]{best} =~ /^(NN|NE|FM)/) {
          complain "$w[$_]{word}/$w[$_]{best} in front of noun!\n";
          my $attr = $w[$_]{best};
          $attr =~ s/S/AT/;
          prefer $w[$_], $attr;
        }
      }
    }
  }

  # regenerate lines
  $i = 0;
  for my $w (@words) {
    my $line = "$$w{word}";
    for (sort {$$w{scores}{$b} <=> $$w{scores}{$a}} keys %{$$w{scores}}) {

      # save as much space as possible
      my $score = $$w{scores}{$_};
      if(1.0 == $score) {
        $score = 1;
      } elsif($score >= 0.01) {
        $score = sprintf "%.3f", $score;
      } else {
        $score = sprintf "%.3e", $score;
      }
      $line .= "\t$_\t$score";
    }
    print "$line\n";
  }
}

# look for the tagger binary
my $base = '';
my $cmdline;
for ($ENV{TNTDIR}, qw(/opt/pkg /data/linux/opt /usr/local /usr)) {
  if ($tagger eq 'tnt') {
    if (-x "$_/tnt/tnt") {
      $base = "$_/tnt";
    } elsif (-x "$_/tnt") {
      $base = $_;
    }
    if($base) {

      my $model = $opt_m || 'negra';

      # use an extra lexicon tuned to our grammar, if present
      my $lex = "$base/models/papa.lex";
      my $EXTRALEXICON = -f $lex ? "-b$lex" : '';
      if($opt_b) {
        $EXTRALEXICON = '';
      }
      $cmdline="$base/tnt $EXTRALEXICON -v0 -z200 $base/models/$model";
      last;
    }
  } elsif ($tagger eq 'TreeTagger') {
    if (-x "$_/TreeTagger/bin/tree-tagger") {
      $base="$_/TreeTagger";
      $cmdline="$base/bin/tree-tagger -quiet -token -threshold 0.01 -prob $base/lib/german.par "
    }
  }
  else {
      $cmdline=$tagger
  }
}

die "Can't find tagger `$tagger'\n" unless $cmdline;

my $in = "/tmp/tagger.in.$$";
my $out = "/tmp/tagger.out.$$";

sub open_file {
  open(IN, ">$in") or die "Can't write `$in': $!\n";
}

sub do_call {
  close(IN);

  # open a tagger with that file
  system "$cmdline $in > $out";

  # read output
  my @output = ();
  open(OUT, "$out") or die "Can't read `$out': $!\n";
  while (<OUT>) {
    push @output, $_;
    if (/^$/) {
      # remove obvious errors
      postprocess \@output;
      @output = ();
    }
  }
  postprocess \@output;


  close(OUT);
  unlink $in, $out;
}

open_file;
my $lines = 0;
while(<>) {
  $lines++;
  print IN;
  if($lines == $blocksize) {
    do_call;
    $lines = 0;
    open_file;
  }
}
do_call;
