#! /usr/bin/env python
# -*- coding:utf-8 -*-
# get errror types of tagging results
# Copyright (C) 2010  Arne Köhn, Niels Beuck

# This file is part of POSeval.

# POSeval is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# POSeval is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with POSeval. If not, see <http://www.gnu.org/licenses/>.

def get_errors(goldfile, testfile):
    numgold = {}
    numtest = {}
    errors = {}
    while True:
        # get the tags - gt = gold, tt = test
        try:
            gl = goldfile.readline()
            tl = testfile.readline()
            if not gl:
                break # eof
            (gword, gt) = gl.strip().split(" ")
            (tword, tt) = tl.strip().split(" ")
            if tword != gword: # just to be safe
                exit("words don't match!")
        except:
            continue # empty line
        try:
            numgold[gt] += 1
        except:
            numgold[gt] = 1
        try:
            numtest[tt] += 1
        except:
            numtest[tt] = 1
        if (gt != tt):
            try:
                errors[gt+" tagged as "+tt] += 1
            except:
                errors[gt+" tagged as "+tt] = 1
    return (numgold, numtest, errors)

def compareErrors(errors1, errors2):
    total1 = 0
    total2 = 0

    #print errors2

    diff = {}

    for e, n in errors1.iteritems():
        total1 += n

    for e, n in errors2.iteritems():
        total2 += n

    for e1, n1 in errors1.iteritems():
        n2=0
        try: 
            n2 = errors2[e1]
        except:
            pass
        #print e1, n2
        diff[e1] = 100* float(n1 - n2) / (total1 - total2) 

    return (total1 , total2, sorted(diff.iteritems(), key= itemgetter(1), reverse=True))


if __name__ == "__main__":
    import os,sys

    if len(sys.argv) == 1:
        print"""
get-errortypes.py <goldfile> <testfile>
get-errortypes.py <goldfile> <testfile1> <testfile2>

first case: print the type of errors that are made in testfie

second case: print errors that have increased most in testfile2 from testfile1
"""
        exit(0)


    from operator import itemgetter
    goldfile = open(os.path.abspath(sys.argv[1]))

    if len(sys.argv) == 3:
        # compare gold with test
        testfile = open(os.path.abspath(sys.argv[2]))
        (g,t,e) = get_errors(goldfile, testfile)
        for error in sorted(e.iteritems(), key=itemgetter(1), reverse=True):
            print error
    else:
        # diff two outputs wrt gold
        testfile1 = open(os.path.abspath(sys.argv[2]))
        testfile2 = open(os.path.abspath(sys.argv[3]))
        (g1,t1,e1) = get_errors(goldfile, testfile1)
        goldfile.seek(0)
        (g2,t2,e2) = get_errors(goldfile, testfile2)
        (t1, t2, result) = compareErrors(e1,e2)
        for error in result:
            print error
