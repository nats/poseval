#! /usr/bin/env python
# -*- coding:utf-8 -*-
# author: Arne köhn <arne@arne-koehn.de>
# license: GPLv3+

# This script takes a list of files from stdin and converts them from
# cdg format to "word tag" files so they are usable for testing
# taggers.
from __future__ import with_statement

import os,sys,re

try:
    datadir = os.path.abspath(sys.argv[1])
    files = os.listdir(datadir)
except:
    # no files given
    files = [x.strip() for x in sys.stdin.readlines()]

wordre = re.compile(r" +\d+ \d+ ([^ \n]+)")
tagre = re.compile(r"cat / ([^ \n]+)")

for f in files:
    try:
        with open(f) as fh:
            needword = 1
            while True:
                l = fh.readline()
                if not l:
                    break # eof
                if needword:
                    m = wordre.search(l)
                    if m:
                        w = m.groups()[0]
                        if w.startswith("'"):
                            w = w[1:-1] # strip single quotes from 'foo-bar'
                        print w,
                        needword = 0
                else:
                    m = tagre.search(l)
                    if m:
                        w = m.groups()[0]
                        if w.startswith("'"):
                            w = w[2:-1] # strip single quotes and $ -> '$.'
                        print w
                        if w == ".":
                            print # extra newline for eos
                        needword = 1
    except:
        pass # who needs robust programs anyway?
    
    
