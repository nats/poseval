#! /usr/bin/env python
# -*- coding:utf-8 -*-
# An evaluator for cda files without good documentation
# Copyright (C) 2010  Niels Beuck <beuck@informatik.uni-hamburg.de>,
#                     Arne Köhn   <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
args = sys.argv[1:]
if len(args) == 0:
    print """usage: tagger_nonmonotonicity.py [test] [gold] ..."""
    exit
if (len(args)) % 2:
    print """you must specify an even number of files!"""
    exit

correct_final = 0
count = 0
stability = [0,0,0,0,0,0]
correct =   [0,0,0,0,0,0]
total   =   [0,0,0,0,0,0]

delay = 5

#print "start"

def parseTestFile(name):
    ret = [] # list of sentences
    sent = [] # list of increments
    incr =[] # list of word-tag pairs
    lastline = ""
    f = open (name)
    for line in f:
        line = line.strip()
        if lastline == line == "":
            # nothing interesting.
            continue
        lastline = line
        if line == "":
            if len(incr) == 1: # new sentence
                if len(sent)>0: # not initial value
                    ret.append(sent)
                sent = [incr]
                incr = []
            else:
                sent.append(incr)
                incr = []
            continue
        else:
            try:
                word, tag = line.rsplit(" ",1)
            except:
                print line, "ERRORERRROR"
            incr.append([word, tag])
    if len(incr) > 0:
        sent.append(incr)
        ret.append(sent)
    return ret

def parseGoldFile(name):
    ret = [] # list of sentences
    sent = [] # list of word-tag pairs
    s = ""
    f = open (name)
    for line in f.readlines():
        line = line.strip()
        if line == "":
            ret.append(sent)
            sent = []
            continue
        word, tag = line.rsplit(" ",1)
        sent.append([word, tag])
    return ret

tests = parseTestFile(args[0])
golds = iter(parseGoldFile(args[1]))
# iterate over all sentences
for test in tests:
    gold = golds.next()
    
    n = len(test)

    # evaluate the final increment first, to evaluate final accuracy
    for i in xrange(n):
        if test[n-1][i][0] != gold[i][0]:
            print "error! test and gold are out of sync"
        if test[n-1][i][1] == gold[i][1]:
            correct_final += 1

    # iterate over all increments
    for i in xrange(n + delay):
        #print "increment" , i

        if i >= n:
            # there is no such  increment of this number
            # we just need this run through to fill the arrays
            i_cap = n-1
        else:
            i_cap=i

        # loop through all tags in the increment
        for j in xrange(i_cap+1):
            #print "  tag " , j
            
            if test[i_cap][j][0] != gold[j][0]:
                print "error! test and gold are out of sync"

            d = i-j

            #print "  d", d
            
            if d > delay:
                # word to far back, skip it
                continue

            curtag= test[i_cap][j][1]

            #determine whether the tag is changed later on
            changed = 0
            for k in xrange(i_cap+1, n):
                nexttag= test[k][j][1]
                if curtag != nexttag:
                    changed =1
                    break
            
            if changed == 0:
                stability[d]+= 1

            # determine whether the tag is correct
            if curtag == gold[j][1]:
                correct[d] += 1
            
            total[d] += 1

    count += n

print "count: ", count
print "correct: ", correct_final
print "accuracy: ", correct
print "stable: ", stability
print "total: ", total

