#! /usr/bin/env python

# args: test gold numtags

import sys, os

if len(sys.argv) != 4:
    print """
usage: multianalyse.py <goldfile> <testfile> <numtags>

goldfile: file with correctly tagged words
testfile: file that contains the output of a tagger.
          Each word can be tagged multiple times, with
          tags ordered by probability.
          Example:  Arne NE NN ADJD
numtags:  number of tags that are looked at. A tagging is
          considered correct iff the correct tag appears in
          the first <numtags> tags.

output:  number of correct tags, number of words
"""

gold = open(sys.argv[1])
test = open(sys.argv[2])
numtags = int(sys.argv[3])


correct = 0
count     = 0

for gl in gold.readlines():
    tl = test.readline()
    if gl == "\n":
        continue
    try:
        word, tag = gl.rsplit(" ",1)
    except:
        print "ERROR:", gl
    tag = tag.strip()
    wtags = tl.split()
    if wtags[0] != word:
        sys.stderr.write("words dont match"+word+" versus "+wtags[0]+"\n")
    if tag in wtags[1:numtags+1]:
        correct += 1
    count += 1

print correct, count
