#! /usr/bin/env python

from sqlobject import *
import sys,os,re

db_filename = os.path.abspath(sys.argv[1])

connection_string = 'sqlite:' + db_filename
connection = connectionForURI(connection_string)
sqlhub.processConnection = connection


class Heise(SQLObject):
    class sqlmeta:
        lazyUpdate = True
    tagger = StringCol()
    tset = IntCol()
    tagtime = FloatCol()
    setuptime = FloatCol()
    result = FloatCol()
    infoblob = StringCol()
    createdOn = TimestampCol()
    unknown = FloatCol()
    knownambig = FloatCol()
    knownunambig = FloatCol()

unknown = re.compile(r'==== unknown =*\n\s*\d+\s+\d+\s+(\d+\.?\d*)%')
knownambig = re.compile(r'--- known ambiguous tokens -*\n\s*\d+\s+\d+\s+(\d+\.?\d*)%')
knownunambig = re.compile(r'--- known unambiguous tokens -*\n\s*\d+\s+\d+\s+(\d+\.?\d*)%')

for res in Heise.select():
    print res.tagger
    res.unknown = float(unknown.search(res.infoblob).groups()[0])
    res.knownambig = float(knownambig.search(res.infoblob).groups()[0])
    res.knownunambig = float(knownunambig.search(res.infoblob).groups()[0])
    res.syncUpdate()
print len(res)

# inst.syncUpdate()

# unknown.search(Results.get(1).infoblob).groups()[0]
