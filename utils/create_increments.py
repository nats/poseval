#! /usr/bin/env python
# -*- coding:utf-8 -*-

# author: Arne Köhn <arne@arne-koehn.de>
# Do what you want with this, or use GPLv3.

# usage: create_increments.py <inputfile >outputilfe

# inputfile contains sentences (one word per line, empty line = new
# sentence)
# output is every increment of every sentence

import sys


buffer = []
for l in sys.stdin:
    if l == "\n":
        # buffer handling
        for i in xrange(len(buffer)):
            print "".join(buffer[:i+1])
        buffer = []
    buffer.append(l)
