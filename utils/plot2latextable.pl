#!/usr/bin/perl -w
# plot2latextable.pl --- Converts the gnuplot plot files to a latex table
# Author: Arne Köhn <arne@arne-koehn.de>
# Created: 05 Oct 2009
# Version: 0.01

use warnings;
use strict;

my $dir = $ARGV[0];

chdir $dir;
my @files = split(/^/m,`ls | grep -v plot`);

foreach  my $tagger (@files) {
    chomp $tagger;
    print $tagger;
    open my $fh, '<', $tagger;
    
    while (<$fh>) {
        $_ =~ s/\d+ ([^ ]+) .*/$1/;
        chomp;
        print " & ", sprintf("%.3f",$_);
    }
    print " \\\\ \n";
    
    
}
print $files[1];


    


__END__

=head1 NAME

plot2latextable.pl - Describe the usage of script briefly

=head1 SYNOPSIS

plot2latextable.pl [options] args

      -opt --long      Option description

=head1 DESCRIPTION

Stub documentation for plot2latextable.pl, 

=head1 AUTHOR

Arne Köhn, E<lt>arne@arne-koehn.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 by Arne Köhn

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

Do what you want with this Program. GPLv3 or Later, Apache, PD...

=head1 BUGS

None reported... yet.

=cut
