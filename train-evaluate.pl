#!/usr/bin/perl -w
# train-evaluate.pl --- Script to train a tagger and then evaluate it.
# Author: Arne Köhn <arne@arne-koehn.de>
# Created: 14 May 2009

use DBI;
use warnings;
use strict;
use Getopt::Long;
use Curses::UI;
use threads;
use threads::shared;
use Thread::Queue;
# This imports POSTaggerEvaluator::NegraProcess
require 'negra-sentence-selector.pl';

my $corpus;
my $do_train;
my $do_evaluate;
my $tagger;
my $numsentences;
my $pick_random;

my $cui = Curses::UI->new (clear_on_exit => 1, -color_support => 1);


sub exit_dialog() {
    my $return = $cui->dialog(
        -message   => "Do you really want to quit?",
        -title     => "Are you sure???",
        -buttons   => ['yes', 'no'],
    );
    die "FIXME: Shut things down properly" if $return;
}

my $menu_tsets = [
    { -label => 'create tsets', -value => \&create_tset},
    { -label => 'evaluate on external file', -value => \&schedule_test_extern},
];

my $menu_taggers = [
    { -label => 'train/test taggers'},
    { -label => 'make graph', -value => \&draw_errorbars }
];


my $menu_data = [
    { -label => 'tsets', -submenu => $menu_tsets},
    { -label => 'taggers', -submenu => $menu_taggers},
];

my $menu = $cui->add(
    'menu', 'Menubar',
    -menu => $menu_data,
    -fg => 'blue'
);

my $win1 = $cui->add(
    'win1', 'Window',
    -border => 1,
    -y    => 1,
    -bfg  => 'red',
);

$cui->set_binding(sub {$menu->focus()}, "\cT");
$cui->set_binding( \&exit_dialog , "\cQ");

my $statuslabel = $win1->add(
    'statuslabel', 'Label',
    -text      => 'Hello, world!',
    -bold      => 1,
    -width     => 40,
);

my $taggerbox = $win1->add(
    'taggerbox', 'Listbox',
    -values   => [],
    -multi    => 1,
    -y        => 2,
    -height   => 10,
    -border   => 1,
    -title    => 'your taggers',
    -vscrollbar => 'right',
);

my $tsetbox = $win1->add(
    'tsetbox', 'Listbox',
    -values   => [],
    -multi    => 1,
    -y        => 12,
    -height   => 10,
    -border   => 1,
    -title    => 'your tsets',
    -vscrollbar => 'right',
);

my $buttonbox = $win1->add(
    'buttonbox', 'Buttonbox',
    -buttons => [
        {
            -label => 'train',
            -onpress => \&schedule_train,
        },
        {
            -label => 'test',
            -onpress => \&schedule_test,
        },
    ],
    -y => 22,
);



my %cfg;
# taken from http://docstore.mik.ua/orelly/perl/cookbook/ch08_17.htm
open CONFIG, "train-evaluate.conf";
while (<CONFIG>) {
    chomp;                  # no newline
    s/#.*//;                # no comments
    s/^\s+//;               # no leading white
    s/\s+$//;               # no trailing white
    next unless length;     # anything left?
    my ($var, $value) = split(/\s*=\s*/, $_, 2);
    $cfg{$var} = $value;
}; 

# Connect to the db and create the necessary tables if they don't
# exist already
my $dbh = DBI->connect("dbi:SQLite:dbname=$cfg{'dbfile'}","","");
$dbh->do('CREATE TABLE IF NOT EXISTS tsets
         (id INTEGER PRIMARY KEY ASC,
          numtrain INTEGER,
          created_on DEFAULT CURRENT_TIMESTAMP,
          infoblob DEFAULT NULL);');
# We don't need this table for now - maybe add it later again.
#$dbh->do('CREATE TABLE IF NOT EXISTS taggers
#         (id INTEGER PRIMARY KEY ASC, name);');

$dbh->do('CREATE TABLE IF NOT EXISTS train
          (id INTEGER PRIMARY KEY ASC,
           tagger TEXT,
           tset INTEGER);');
$dbh->do('CREATE TABLE IF NOT EXISTS results
           (id INTEGER PRIMARY KEY ASC,
            tagger    TEXT,
            tset      INTEGER,
            tagtime   REAL,
            setuptime REAL,
            result    REAL,
            infoblob  TEXT,
            created_on DEFAULT CURRENT_TIMESTAMP);');

my @taggers;
@taggers=split(/,/,$cfg{'taggers'});

## BEGIN JOBMANAGEMENT
my $num_running :shared = 0;
my $pending_jobs = Thread::Queue->new();
my $job_results  = Thread::Queue->new();
my $free_hosts   = Thread::Queue->new();

# We keep track of all threads to join them later.
my @threadlist;

sub manage_jobs {
    my $num_pending = $pending_jobs->pending();
    while ( $num_pending and $free_hosts->pending()) {
        my $host = $free_hosts->dequeue();
        my $job = $pending_jobs->dequeue();
        if ($job->{'type'} eq 'train') {
            push @threadlist,
                threads->create(\&train_tagger, $job->{'tagger'}, $job->{'tset'}, $host);
        }
        if ( $job->{'type'} eq 'test') {
            push @threadlist,
                threads->create(\&test_tagger, $job->{'tagger'}, $job->{'tset'}, $host);
        }
        if ( $job->{'type'} eq 'testextern') {
            push @threadlist,
                threads->create(\&test_tagger, $job->{'tagger'}, $job->{'tset'}, $host, $job->{'extern'});
        }
        $num_running++;
        $num_pending = $pending_jobs->pending();
    }
    if ( $num_pending or $num_running) {
        $statuslabel->text("$num_running Jobs running, $num_pending Jobs pending,");
    } else {
        $statuslabel->text("No jobs running...");
        while (my $t = pop @threadlist) {
            $t->join();
        }
    }
    while ( $job_results->pending()) {
        my $job = $job_results->dequeue();
        process_jobresult($job);
    }
}

sub process_jobresult {
    my $job = shift;
    $free_hosts->enqueue($job->{'host'});
    $num_running--;
    my $type = $job->{'type'};
    my $tagger = $job->{'tagger'};
    my $tset   = $job->{'tset'};
    if ( $type eq 'train' ) {
        $dbh->do("INSERT INTO train (tagger, tset) VALUES ('$tagger', $tset);");
    } 
    elsif ( $type =~ /test/ ) {
        my $table;
        if ( $type eq 'test' ) {
            $table = 'results';
        } elsif ( $type eq 'testextern' ) {
            $table = $job->{'extern'}
        }
        my $tagtime = $job->{'tagtime'};
        my $setuptime = $job->{'setuptime'};
        # FIXME warn if svmt-standard is not trained on this tset
        my $info = $job->{'info'};
        $info =~ m/=== OVERALL ACCURACY ===.*? (\d+\.\d+)%/s;
        my $result = $1;
        $dbh->do("INSERT INTO $table
                  (tagger, tset, tagtime, setuptime, result, infoblob) VALUES
                ('$tagger','$tset','$tagtime','$setuptime','$result','$info');");
    }
        die "negative running jobs?!" if ( $num_running<0);    
}

$cui->set_timer('manage_jobs',\&manage_jobs,1);
## END JOBMANAGEMENT



$taggerbox->values(@taggers);
$free_hosts->enqueue(split(/,/,$cfg{'hosts'}));

sub update_tsetbox {
    my $sth = $dbh->prepare('SELECT * FROM tsets ORDER BY numtrain');
    my %tsethash;
    my @keys;
    $sth->execute();
    while (my $result = $sth->fetchrow_hashref) {
        my $id = $result->{'id'};
        push @keys, $id;
        $tsethash{$id}=$result->{'numtrain'}.' - '.$id;
    }
    $tsetbox->values(@keys);
    $tsetbox->labels(\%tsethash);
}

update_tsetbox();

sub schedule_train {
    foreach my $tagger ($taggerbox->get() ) {
        foreach my $tset ($tsetbox->get()) {
            if ( ! $dbh->selectrow_array("SELECT id FROM train
                                        WHERE tagger='$tagger' AND tset=$tset")){
                $pending_jobs->enqueue({'tagger'=>$tagger, 'tset'=>$tset, 'type'=>'train'});
            }
        }
    }
    manage_jobs();
}

sub schedule_test {
    foreach my $tagger ($taggerbox->get() ) {
        foreach my $tset ($tsetbox->get()) {
            if ( ! $dbh->selectrow_array("SELECT id FROM results
                                        WHERE tagger='$tagger' AND tset=$tset")){
                $pending_jobs->enqueue({'tagger'=>$tagger, 'tset'=>$tset, 'type'=>'test'});
            }
        }
    }
    manage_jobs();
}

sub schedule_test_extern {
    my $extern = $cui->question(-question=>'Which external file to evaluate on?');
    $dbh->do("CREATE TABLE IF NOT EXISTS $extern
           (id INTEGER PRIMARY KEY ASC,
            tagger    TEXT,
            tset      INTEGER,
            tagtime   REAL,
            setuptime REAL,
            result    REAL,
            infoblob  TEXT,
            created_on DEFAULT CURRENT_TIMESTAMP);");

    foreach my $tagger ($taggerbox->get() ) {
        foreach my $tset ($tsetbox->get()) {
            if ( ! $dbh->selectrow_array("SELECT id FROM $extern
                                        WHERE tagger='$tagger' AND tset=$tset")){
                $pending_jobs->enqueue({'tagger'=>$tagger, 'tset'=>$tset, 'type'=>'testextern', 'extern'=>$extern});
            }
        }
    }
    manage_jobs();
}

sub train_tagger {
    my $tagger = shift;
    my $tset = shift;
    my $host = shift;
    # TODO: measure time of execution
    my $pwd = `pwd`;
    chomp $pwd;
    my $command = "sh $pwd/taggers/$tagger/train $pwd/tfiles/$tset/train $tset";
    if ( not ($host eq 'local')) {
        $command = "ssh $host $command";
    }
    print STDERR "running ",$command;
    `$command`;
    $job_results->enqueue({'type'=>'train','tagger'=>$tagger,'tset'=> $tset,'host'=> $host});
}

sub test_tagger {
    my $tagger = shift;
    my $tset = shift;
    my $host = shift;
    my $extern = shift; # is this an external file not deriving from
                        # our corpus? yes -> tag $extern instead of
                        # tfiles/$tset/test.
    my $pwd = `pwd`;
    chomp $pwd;

    my $testfile;
    my $outputfile;
    my $goldfile;
    my $type;
    if ( $extern ) {
        $testfile = "$pwd/tfiles-extern/$extern";
        $outputfile = "$pwd/tmp/$tagger-$tset-$extern";
        $goldfile = "$pwd/tfiles-extern/$extern-gold";
        $type = 'testextern';
    } else {
        $testfile = "$pwd/tfiles/$tset/test";
        $outputfile = "$pwd/tmp/$tagger-$tset";
        $goldfile = "$pwd/tfiles/$tset/gold";
        $type = 'test';
    }
    my $command = "sh $pwd/taggers/$tagger/tag $tset $testfile $outputfile";
    if ( not ($host eq 'local')) {
        $command ="ssh $host $command";
    }
    print STDERR "running ",$command;
    my $time = `$command`;
    $command = "$cfg{'svmteval'} 0 $pwd/taggers/svmt-standard/models/$tset $goldfile $outputfile";
    if ( not ($host eq 'local')) {
        $command ="ssh $host $command";
    }
    print STDERR "running ",$command;
    my $info = `$command`;
    my $setuptime = -1;
    my $tagtime = -1;
    if ($time =~ m/SETUPTIME=(\d+)/) {
        $setuptime = $1
    }
    if ($time =~ m/TAGTIME=(\d+)/) {
        $tagtime = $1
    }
    $job_results->enqueue({'type'=>$type,
                           'tagger'=>$tagger,
                           'tset'=> $tset,
                           'host'=> $host,
                           'tagtime'=> $tagtime,
                           'setuptime'=> $setuptime,
                           'info'=>$info,
                           'extern'=>$extern,
                       });
}

sub get_or_restore_tset {
    my $t_name = shift;
    die "not yet implemented, should return the filenames for the test and train set";
}


sub list_testresults {
    die "should return information about all tsets";
}

sub create_tset {
    my $sizestr = $cui->question(-question=>'How many sentences for training?');
    eval(my $size=int($sizestr)) or do
        {
            $cui->error(-message => 'You have to enter a number!');
            return;
        };

    # I'm lazy here since we don't have race-conditions. Be warned if
    # you copy-paste!
    $dbh->do("INSERT INTO tsets (numtrain) VALUES ($size)");
    my $rv = $dbh->last_insert_id('','','tsets','');
    open my $cfile, $cfg{'corpus'};

    # pwd inserts newlines, strip it!
    my $pwd = `pwd`;
    chomp $pwd;
    `mkdir ${pwd}/tfiles/${rv}/`;
    open my $train, ">", $pwd.'/tfiles/'.$rv.'/train';
    open my $test, '>', $pwd.'/tfiles/'.$rv.'/test';
    open my $gold, '>', $pwd.'/tfiles/'.$rv.'/gold';
    POSTaggerEvaluator::NegraProcess::process(1,
                                              $cfile,
                                              $train,
                                              $gold,
                                              $test,
                                              $size,
                                              1,
                                              0
                                           );
    close $test;
    close $train;
    update_tsetbox();
}

### BEGIN DRAWING FOO

sub draw_errorbars {
    my @taggers = $taggerbox->get();
    my $dir = $cui->question(-question=>'Output directory of the graph?');
    my $table = $cui->question(-question=>'which table to plot?');
    my $column = $cui->question(-question=>'which column to plot?');
    if ( $column eq '' ) {
        $column = 'result';
    }
    if ( $table eq '') {
	$table = 'results';
    }
    `rm -rf graphs/$dir`;
    `mkdir graphs/$dir`;
    open my $gnuplotfile, ">", "graphs/$dir/plot";
    print $gnuplotfile "plot ";
    my $res;
    my $set_comma = 0;
    my $foobar = "";
    for my $tagger (@taggers) {
	$res = $dbh->selectall_arrayref(        
	            "SELECT
                        numtrain,
                        AVG($column) AS avg,
                        MIN($column) AS min,
                        MAX($column) AS max
                     FROM
                        $table, tsets
                     WHERE
                        $table.tset=tsets.id
                        AND tagger='$tagger'
                     GROUP BY numtrain
                     ORDER BY numtrain;",
               );
        open my $f, ">", "graphs/$dir/$tagger";
        for my $r (@$res) {
            print $f join " ",@$r;
            print $f "\n";
        }
        if ($set_comma) {
            print $gnuplotfile ", ";
        }
        $set_comma = 1;
        print $gnuplotfile "\"$tagger\" with errorbars";
    }
    print $gnuplotfile "\n";
    `cd graphs/$dir; gnuplot -persist plot`
}

$cui->mainloop;

__END__

=head1 NAME

train-evaluate.pl - train and evaluate taggers.

=head1 SYNOPSIS

perl train-evaluate.pl

=head1 DESCRIPTION

start it, use ctrl-t to open the menu, use ctrl-q to quit.

=head1 AUTHOR

Arne Köhn, E<lt>arne@arne-koehn.eeE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 Arne Köhn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 BUGS

Curses::UI sometimes segfaults - doesn't seem to be my fault and it's not really problematic.

=cut
